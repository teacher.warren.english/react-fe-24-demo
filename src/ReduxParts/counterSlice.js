import { createSlice } from "@reduxjs/toolkit"

export const counterSlice = createSlice({
    name: "counter",
    // initialState: 100, // old way with primitive number
    initialState: {
        value: 100,
    },
    reducers: {
        // actions
        increment: (state) => {
            console.log("counterSlice increment() fired!");
            state.value += 1
        },
        decrement: (state) => {
            console.log("counterSlice decrement() fired!");
            state.value -= 1
        },
        increaseByValue: (state, action) => {
            console.log("increaseByValue() fired!");
            console.log("ACTION PAYLOAD: ", action.payload)
            state.value += action.payload
        }
    }
})

export const { increment, decrement, increaseByValue } = counterSlice.actions
export default counterSlice.reducer