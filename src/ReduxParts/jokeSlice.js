import { createAsyncThunk, createSlice } from "@reduxjs/toolkit"
import { JOKE_API_URL } from '../utils'

export const getJokeAsync = createAsyncThunk(
    "joke/getJokeAsync",
    async () => {
        try {
            const response = await fetch(JOKE_API_URL)
            const jsonData = await response.json()

            return jsonData
        }
        catch (error) {
            console.error(error)
        }
    }
)

export const jokeSlice = createSlice({
    name: "joke",
    initialState: {
        setup: "Generic setup",
        delivery: "Generic punchline",
    },
    reducers: {
        // reducer stuff (empty)
    },
    extraReducers: (builder) => {
        builder
            .addCase(getJokeAsync.fulfilled, (state, action) => {
                // logging out the action payload received from thunk function
                console.log(action.payload)

                // modifying state
                state.setup = action.payload.setup
                state.delivery = action.payload.delivery
            })
            .addDefaultCase((state, action) => {

            })
    }
})

export default jokeSlice.reducer