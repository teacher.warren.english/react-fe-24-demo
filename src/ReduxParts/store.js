import { configureStore } from "@reduxjs/toolkit"
import counter from './counterSlice'
import joke from './jokeSlice'

export default configureStore({
    reducer: {
        counter,
        // perhaps we'll have more reducers (more pieces of state to manage)
        joke,
    },
})