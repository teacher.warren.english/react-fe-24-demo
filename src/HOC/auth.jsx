import { useContext } from "react"
import { UserContext } from "../Context/UserContext"
import { Navigate } from 'react-router-dom'

const auth = Component => props => {

    // authentication logic
    const [username, ] = useContext(UserContext)

    if (!username) {
        // failure -> redirect the user to the home page
        console.log("You're not logged in, redirecting you back to the login page");
        return <Navigate to='/' />
    } else {
        // success -> show the component
        return <Component {...props} />
    }
}

export default auth