export const employeesArray = [ "Joe", "Mary", "Kirsten", "Stephen", "Barry" ]

export const employeeObjectArray = [
    {
        id: 1,
        fname: "Joe",
        lname: "Soap",
        dob: "1994-12-05",
    },
    {
        id: 2,
        fname: "Mary",
        lname: "Jane",
        dob: "1985-06-06",
    },
    {
        id: 3,
        fname: "Kirsten",
        lname: "Garfield",
        dob: "1987-07-16",
    },
    {
        id: 4,
        fname: "Stephen",
        lname: "Martin",
        dob: "1991-02-22",
    },
    {
        id: 5,
        fname: "Barry",
        lname: "Shiraz",
        dob: "1990-10-14",
    },
]