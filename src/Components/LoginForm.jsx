import { useContext, useState } from "react"
import { UserContext } from "../Context/UserContext"
import { useNavigate } from 'react-router-dom'

function LoginForm() {

    // HOOKS
    const [inputUsername, setInputUsername] = useState("")
    const [, setUsername] = useContext(UserContext)

    const ourNavigator = useNavigate()


    function handleLogin() {
        // check that username is not empty
        if (checkUsernameEmpty()) {
            // set lifted user state to inputted username
            setUsername(inputUsername)
    
            // redirect to jokes page
            ourNavigator('/jokes')
        }
        else {
            // handle the error
            console.log("Username could not be validated.");
            setInputUsername("")
        }
    }

    function checkUsernameEmpty() {
        if (!inputUsername.trim())
            return false
        
        return true
    }

    function handleSetInputUsername(event) {
        setInputUsername(event.target.value);
    }

    return (
        <>
            <label htmlFor="username">Username: </label>
            <input onChange={ handleSetInputUsername } type="text" placeholder="warren-west" id="username" value={ inputUsername } />
            &nbsp;
            <button onClick={ handleLogin }>Login</button>
        </>
    )
}

export default LoginForm