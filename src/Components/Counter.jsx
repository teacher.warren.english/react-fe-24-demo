import { useSelector, useDispatch } from 'react-redux'
import { increment, decrement, increaseByValue } from '../ReduxParts/counterSlice'

function Counter() {
    // Hooks
    const count = useSelector((state) => state.counter.value)
    const dispatch = useDispatch()

    // function doStuff() {
    //     // some stuff before the dispatch
    //     dispatch(increase())
    //     // som stuff after the dispatch
    // }

    return (
        <>
            <h3>Counter: { count }</h3>
            <button onClick={() => dispatch(decrement())}>➖</button>
            &nbsp;
            <button onClick={() => dispatch(increment())}>➕</button>
            &nbsp;
            <button onClick={() => dispatch(increaseByValue(25))}>🚀</button>
        </>
    )
}

export default Counter