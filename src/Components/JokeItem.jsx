import { useEffect, useState } from "react"


function JokeItem(props) {

    // Maybe hide delivery until a button is clicked
    const [showDelivery, setShowDelivery] = useState(false)

    function handleShowDelivery() {
        setShowDelivery(true)
    }

    useEffect(() => {
        setShowDelivery(false)
    }, [props.currentJoke]) // dependency for useEffect to fire is currentJoke changing

    return (
        <>
            <p><b>{ props.currentJoke.setup }</b></p>
            <button onClick={ handleShowDelivery }>Show punchline</button>
            { showDelivery && <p>{ props.currentJoke.delivery }</p>}
        </>
    )
}

export default JokeItem