import './Navbar.css'
import { NavLink } from 'react-router-dom'

function Navbar() {


    return (
        <ul className='navbar-container'>
            <li className='navbar-item'><NavLink to='/' >Home</NavLink></li>
            <li className='navbar-item'><NavLink to='/animals' >Animals</NavLink></li>
            <li className='navbar-item'><NavLink to='/jokes' >Jokes</NavLink></li>
            <li className='navbar-item'><NavLink to='/employees' >Employees</NavLink></li>
            <li className='navbar-item'><NavLink to='/cars' >Cars</NavLink></li>
            <li className='navbar-item'><NavLink to='/counter' >Counter</NavLink></li>
        </ul>
    )
}

export default Navbar