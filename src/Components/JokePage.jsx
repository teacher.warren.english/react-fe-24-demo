import { useEffect } from "react"
import { useDispatch, useSelector } from "react-redux"
import JokeItem from "./JokeItem"
import auth from "../HOC/auth"
import { getJokeAsync } from "../ReduxParts/jokeSlice"

function JokePage() {
    //const [jokeData, setJokeData] = useState({})

    // React HOOKS -> useEffect()
    const jokeData = useSelector(state => state.joke)
    const dispatch = useDispatch()

    useEffect(() => {
        getApiJokeDataAsync()
    }, []) // will only run once!

    // logic for fetching joke data from the API
    async function getApiJokeDataAsync() {
       // replaced by thunk

        dispatch(getJokeAsync())

       console.log("Replaced by thunk");
    }

    return (
        <>
            <JokeItem currentJoke={ jokeData } />
            <button onClick={ getApiJokeDataAsync }>Get next joke</button>
        </>
    )
}

export default auth(JokePage)