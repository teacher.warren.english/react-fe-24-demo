import { useState } from "react"

function Car() {

    // React Hooks
    const [car, setCar] = useState({
        id: 1,
        brand: "Ford",
        model: "Focus",
        weight: 750,
        color: "Red",
        numWheels: 4,
        spareTire: true,
    })

    // Event Handlers
    function handleToggleSpareTire() {
        let newTireValue = !car.spareTire

        setCar({
            // spread operator
            ...car,
            spareTire: newTireValue,
        })
    }

    return (
        <>
            <h4>{car.brand} {car.model} ({car.color})</h4>
            <p>Weight: {car.weight} Wheels: {car.numWheels}</p>
            {car.spareTire && <p>🛞</p>}

            <button onClick={handleToggleSpareTire}>Toggle</button>
        </>
    )
}

export default Car