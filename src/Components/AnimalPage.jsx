import Animal from "./Animal"

function AnimalPage() {

    const animalName = "Joey"

    return (
        // Animal Logical Component
        <Animal theAnimalName={ animalName } />
    )
}

export default AnimalPage