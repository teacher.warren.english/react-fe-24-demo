function EmployeeListItem({currentEmployee}) {


    return (
        <>
            <h3>{ currentEmployee.fname } { currentEmployee.lname } </h3>
            <small>{ currentEmployee.dob }</small>
        </>
    )
}

export default EmployeeListItem