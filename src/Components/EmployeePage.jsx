import auth from "../HOC/auth"
import { employeeObjectArray } from "../employeeList"
import EmployeeListItem from "./EmployeeListItem"

function EmployeePage() {

    // HOFs:
    // Array.Prototype.filter() -> return a subset of an array (none, one or many)
    // Array.Prototype.find() -> returns the first match in an array
    // Array.Prototype.reduce() -> returns a single value, that is an aggregate of the array
    // Array.Prototype.map() -> transforms an array from one form to another

    let newArray = employeeObjectArray.map((item) => {
        return <EmployeeListItem currentEmployee={ item } key={ item.id } />
    })

    console.log(newArray);

    return (
        <>{ newArray }</>
    )
}

export default auth(EmployeePage)