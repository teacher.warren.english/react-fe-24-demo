import { useState } from "react"

function Guitar() {
    // Variables
    const [guitarName, setGuitarName] = useState("Fender Jazzmaster")
    const [numStrings, setNumStrings] = useState(6)
    
    // Event Handler
    function handleButtonChangeClick() {
        console.log("Button was clicked")

        setGuitarName("Gibson Les Paul")
        setNumStrings(8)
        
        console.log("guitarName: ", guitarName)
    }

    // Return VALID JSX
    return (
        <>
            <h1>{ guitarName }</h1>
            <p>Strings: { numStrings }</p>
            <button onClick={handleButtonChangeClick}>Change Variables</button>
        </>
    )
}

export default Guitar