import logo from './logo.svg'
import './App.css'
import GuitarPage from './Components/GuitarPage'
import CarPage from './Components/CarPage'
import AnimalPage from './Components/AnimalPage'
import EmployeePage from './Components/EmployeePage'
import JokePage from './Components/JokePage'
import Navbar from './Components/Navbar'
import LoginPage from './Components/LoginPage'
import CounterPage from './Components/CounterPage'

import { BrowserRouter, Routes, Route } from 'react-router-dom'

function App() {
  return (
    <div className="App">
      <p>Our first React project with routing</p>
      <BrowserRouter>
        <Navbar />
        <Routes>
          <Route path='/' element={<LoginPage />} />
          <Route path='/login' element={<LoginPage />} />
          <Route path='/guitars' element={<GuitarPage />} />
          <Route path='/cars' element={<CarPage />} />
          <Route path='/animals' element={<AnimalPage />} />
          <Route path='/employees' element={<EmployeePage />} />
          <Route path='/jokes' element={<JokePage />} />
          <Route path='/counter' element={<CounterPage />} />
          <Route path='*' element={<h1>Error 404!</h1>} />
        </Routes>
      </BrowserRouter>
    </div>
  )
}

export default App
